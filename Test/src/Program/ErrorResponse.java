package Program;
public class ErrorResponse implements Response{
	private Request originalRequest;
	private Exception originalException;
	ErrorResponse(Request re, Exception ex){
		originalRequest = re;
		originalException = ex;
	}
	public Request getOriginalRequest(){
		return originalRequest;
	}
	
	public Exception getOriginalException(){
		return originalException;
	}
}
